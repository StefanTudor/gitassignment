import java.util.ArrayList;

public class Elev implements CalculeazaScorActivitate{
	String nume;
	String clasa;
	ArrayList<Integer> note;
	int numarDocumenteCitite;
	
	public Elev(String nume, String clasa, ArrayList<Integer> note, int numarDocumenteCitite) {
		this.nume = nume;
		this.clasa = clasa;
		this.note = note;
		this.numarDocumenteCitite = numarDocumenteCitite;
	}
	
	public String getNume() {
		return nume;
	}
	
	public float getMedieNote() {
		if (this.note.size() > 0) {
			float suma = 0;
			for (int nota : this.note)
				suma += nota;

			return suma / this.note.size();
		} else
			return 0;
	}
	
	private float getPuncteProcentualeDocumenteCitite() {
		float puncteProcentuale = 0;
		if(Document.numarDocumente >= numarDocumenteCitite && numarDocumenteCitite >= 0) {
			puncteProcentuale = (numarDocumenteCitite / Document.numarDocumente);
		}
		return puncteProcentuale;
	}

	@Override
	public float getScorInProcente() {
		return Math.round((getMedieNote() * 6) + (getPuncteProcentualeDocumenteCitite() * 40));
	}
}
