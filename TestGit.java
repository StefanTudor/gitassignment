import java.util.ArrayList;

public class TestGit {

	public static void main(String[] args) {
		System.out.println("Hello Git ! Denumirea proiectului de licență este:"
				.concat(" Utilizarea tehnicilor de machine learning pentru dezvoltarea unei aplcații educaționale."));

		ArrayList<Integer> note = new ArrayList<>();
		note.add(10);
		note.add(9);
		note.add(7);
		Elev elev = new Elev("Tudor", "12B", note, 1);
		
		Document doc = new Document("doc.txt");
		
		System.out.print("Elevul "
				.concat(elev.getNume())
				.concat(" are scorul ")
				.concat(String.valueOf(elev.getScorInProcente())));
	}

}
